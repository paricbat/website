import { createApp } from "vue";
import App from "./App.vue";
import AboutView from "./views/AboutView.vue";
import { createRouter, createWebHistory } from "vue-router";
import StuffIDidView from "./views/StuffIDidView.vue";

export const routes = [
  {
    path: "/",
    name: "about",
    meta: { title: "About me" },
    component: AboutView,
  },
  {
    path: "/stuff-i-did",
    name: "stuff-i-did",
    meta: { title: "Stuff I Did" },
    component: StuffIDidView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, _from, next) => {
  document.title = `paricbat - ${to.meta.title}`;
  next();
});

createApp(App).use(router).mount("#app");
